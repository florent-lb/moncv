function switchPageSide(pageNameIn,pageNameOut,toSide)
{

    var elementToHide = document.getElementById("block-"+pageNameIn);
    var elementToSee = document.getElementById("block-"+pageNameOut);
    cleanAnimation(elementToHide);
    cleanAnimation(elementToSee);
    if(toSide === 'left')
    {             
      
      elementToHide.style.transform = "translateX(-100%)";
        elementToHide.className = elementToHide.className + " left-out"
        
       elementToSee.style.transform = "none";     
        elementToSee.className = elementToSee.className + " left-in"
    }
    if(toSide === 'right')
    {        
        elementToHide.style.transform = "translateX(100%)";
        elementToHide.className = elementToHide.className + " right-out"
               
        elementToSee.style.transform = "none";
        elementToSee.className = elementToSee.className + " right-in"
    }

 
    
}


function cleanAnimation(elementToClean)
{
    var toRemove = ["left-in","left-out","right-in","right-out"];

    toRemove.forEach(function(e){
        elementToClean.className = elementToClean.className.replace(e,"");
    });
   
}